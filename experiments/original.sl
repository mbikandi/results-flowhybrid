#!/bin/bash
#SBATCH -J dh-negatives
#SBATCH --time=20:00:00
#SBATCH --gres=gpu:a40:1
#SBATCH --ntasks=1
#SBATCH --mem-per-cpu=6G
#SBATCH --output=%x-%j.out
#SBATCH --error=%x-%j.err

cd /gpfs/VICOMTECH/home/mbikandi

srun singularity exec --bind /gpfs/VICOMTECH/Databases/:/datasets/ --nv ./torch-devel.sif /bin/bash <<'EOT'
python3 ./flowhybrid/fine_tune_dl3v.py --dataroot /datasets/SHIFT/discrete-semseg \
--num_classes 12 --class_config 12_pedestrian_ood --batch_size 16 --epochs 5 \
--lr 1e-5 --lr_min 1e-10 --beta 0.01 0 0.1 \
--cp ./experiments/original/checkpoint.pt \
--max_val_steps 1000 --dataset shift_nopedestrian \
--paste_outliers True --paste_outliers_val False \
--horizon 0 --rect_ood_prob 0.5 --alpha_blend 1 \
--pretrained_weights ./weights/nopedestrian_12.pth \
--neg_dataroot /datasets/GeneralDatabases/ADEChallengeData2016 \
--neg_dataset ade
EOT

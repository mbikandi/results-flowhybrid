#!/bin/bash
#SBATCH -J dh-eval-cost_horizon
#SBATCH --time=10:00:00
#SBATCH --gres=gpu:t4:1
#SBATCH --cpus-per-task=1
#SBATCH --ntasks=1
#SBATCH --mem-per-cpu=4G
#SBATCH --output=%x-%j.out
#SBATCH --error=%x-%j.err

cd /gpfs/VICOMTECH/home/mbikandi

srun singularity exec --bind /gpfs/VICOMTECH/Databases/:/datasets/ --nv ./torch-devel.sif /bin/bash <<'EOT'
python3 ./flowhybrid/compute_metrics.py --dataroot /datasets/SHIFT/discrete-semseg --num_classes 12 --class_config 12_pedestrian_ood --weights ./experiments/cost_horizon/model_5.pth --dataset shift_pedestrian --min_ped 10000 --max_ped 15000 --score_functions all
EOT


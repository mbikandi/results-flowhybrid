#!/bin/bash
#SBATCH -J dh-negatives
#SBATCH --time=2-00:00:00
#SBATCH --gres=gpu:a40:1
#SBATCH --ntasks=1
#SBATCH --mem-per-cpu=6G
#SBATCH --output=%x-%j.out
#SBATCH --error=%x-%j.err

cd /gpfs/VICOMTECH/home/mbikandi

srun singularity exec --bind /gpfs/VICOMTECH/Databases/:/datasets/ --nv ./torch-devel.sif /bin/bash <<'EOT'
python3 ./flowhybrid/fine_tune_dl3v.py --dataroot /datasets/SHIFT/discrete-semseg \
--num_classes 13 --class_config 13 --batch_size 16 --epochs 2 \
--lr 1e-5 --lr_min 1e-10 --beta 0.01 0.005 0.02 \
--cp ./all_noblend_pedestrian/checkpoint.pt \
--max_val_steps 1000 --dataset shift \
--paste_outliers True --paste_outliers_val True \
--horizon 0.33 --rect_ood_prob 0 --alpha_blend 1 \
--pretrained_weights ./weights/pedestrian_13.pth \
--neg_dataroot /datasets/GeneralDatabases/ADEChallengeData2016 \
--neg_dataset ade_small
EOT


#!/bin/bash
#SBATCH -J dh-eval-all_noblend_pedestrian
#SBATCH --time=10:00:00
#SBATCH --gres=gpu:t4:1
#SBATCH --cpus-per-task=1
#SBATCH --ntasks=1
#SBATCH --mem-per-cpu=4G
#SBATCH --output=%x-%j.out
#SBATCH --error=%x-%j.err

cd /gpfs/VICOMTECH/home/mbikandi

srun singularity exec --bind /gpfs/VICOMTECH/Databases/:/datasets/ --nv ./torch-devel.sif /bin/bash <<'EOT'
python3 ./flowhybrid/compute_metrics.py --dataroot /datasets/GeneralDatabases/StreetHazards --num_classes 13 --class_config 13 --weights ./experiments/all_noblend_pedestrian/model_2.pth --dataset streethazards --min_ped 10000 --max_ped 15000 --score_functions all
EOT


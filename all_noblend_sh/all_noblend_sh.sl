#!/bin/bash
#SBATCH -J dh-all_noblend_sh
#SBATCH --time=20:00:00
#SBATCH --gres=gpu:a40:1
#SBATCH --ntasks=1
#SBATCH --mem-per-cpu=8G
#SBATCH --output=%x-%j.out
#SBATCH --error=%x-%j.err

cd /gpfs/VICOMTECH/home/mbikandi

srun singularity exec --bind /gpfs/VICOMTECH/Databases/:/datasets/ --nv ./torch-devel.sif /bin/bash <<'EOT'
python3 ./flowhybrid/fine_tune_dl3v.py --dataroot /datasets/SHIFT/discrete-semseg \
--num_classes 13 --class_config 13_streethazards --batch_size 16 --epochs 1 \
--lr 1e-6 --lr_min 1e-8 --beta 0.01 0.005 0.02 \
--cp ./all_noblend_sh/checkpoint.pt \
--max_val_steps 1000 --dataset shift \
--paste_outliers True --paste_outliers_val False \
--horizon 0.33 --rect_ood_prob 0 --alpha_blend 1 \
--pretrained_weights ./pretrain_dl3v_sh/model_5.pth \
--neg_dataroot /datasets/GeneralDatabases/ADEChallengeData2016 \
--neg_dataset ade_small
EOT

